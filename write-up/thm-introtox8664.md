intro to x86-64 (x86-64 어셈블리어의 기초)
# 
tools
- [radare2](https://github.com/radareorg/radare2)

syntax
- [AT&T](http://web.mit.edu/rhel-doc/3/rhel-as-en-3/i386-syntax.html)
#
access
```
ssh tryhackme@ATTACK_BOX_IP
#password: reismyfavl33t
```
![image](https://user-images.githubusercontent.com/61821641/151070932-1de04955-373e-41f9-b879-167715b7338f.png)


#
`introduction`

![image](https://user-images.githubusercontent.com/61821641/151071371-ac37cc39-8337-4b28-8f65-b7bbb6e6e6eb.png)

![image](https://user-images.githubusercontent.com/61821641/151071486-a5aa1a7b-a23e-425a-97c0-55faf56429b1.png)

![image](https://user-images.githubusercontent.com/61821641/151073527-969405dc-54f9-4229-8cc7-5c2ce7467b6c.png)

#
`if`

![image](https://user-images.githubusercontent.com/61821641/151078253-e3543126-8c55-42f4-a3f9-06ea679d2ca7.png)

![image](https://user-images.githubusercontent.com/61821641/151074038-36468467-969c-4b4d-89b4-09a74843a38e.png)

#
`loop`

![image](https://user-images.githubusercontent.com/61821641/151239874-348282d0-3916-4a20-afc8-de4f2de7df71.png)

![image](https://user-images.githubusercontent.com/61821641/151240046-866ef1d2-7524-4243-8c68-819c34352a63.png)

![image](https://user-images.githubusercontent.com/61821641/151240590-904c8815-5d61-4e0f-813b-f4da78c7e40f.png)
