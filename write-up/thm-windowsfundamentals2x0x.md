윈도우 기초 2
#
System Configulation

`MSConfig`를 검색해서 열수있다.

![image](https://user-images.githubusercontent.com/61821641/148648656-3d81e9bf-ed79-4e71-a34d-648ad645203c.png)

- General : 부팅 시 로드할 Windows용 장치 및 서비스를 선택할 수 있다. 옵션은 일반, 진단, 선택. 

![image](https://user-images.githubusercontent.com/61821641/148648745-866cbc29-f01e-47c9-ab5b-bc00a67cc79f.png)

- Boot : 운영 체제에 대한 다양한 부팅 옵션을 정의할 수 있다.

![image](https://user-images.githubusercontent.com/61821641/148648781-626a1865-8caf-4ed4-99ae-6e9fe644cb8e.png)

- Services : 상태(실행 중 또는 중지됨)에 관계없이 시스템에 대해 구성된 모든 서비스가 나열된다. 서비스는 백그라운드에서 실행되는 특수한 유형의 애플리케이션이다.

![image](https://user-images.githubusercontent.com/61821641/148648861-e9082d1a-d037-4cd8-91b4-bcdf1d243873.png)

- Startup : vm에서는 정보를 얻을 게 없다. 위 스크린샷은 로컬의 시스템 구성 시작탭이다.


보다시피 마이크로소프트는 작업 관리자(taskmgr)를 사용하여 시작 항목을 관리(실행/실행)하는 것이 좋다. 시스템 구성 유틸리티는 시작 관리 프로그램이 아니다.(참고: 연결된 VM에 대한 작업 관리자를 열면 작업 관리자에 시작 탭이 표시되지 않는다.)  

![image](https://user-images.githubusercontent.com/61821641/148649037-b61c1c00-203a-4449-a18d-53d72400e191.png)

- Tools : 운영 체제를 추가로 구성하기 위해 실행할 수 있는 다양한 유틸리티(도구) 목록이 있다. 각 툴에 대한 간략한 설명이 있어 툴의 용도를 파악할 수 있다.
    - Selected command 섹션
    - Launch : 도구를 실행하려면 버튼을 클릭하여 도구를 실행할 수 있다.
#
문제
1. Systems Internals를 제조업체로 하는 서비스 이름

![image](https://user-images.githubusercontent.com/61821641/148649144-384a08ac-0f9b-4286-bc87-6fcf99d66181.png)

답 :  PsShutDown


2. Windows License는 누구에게 등록되어 있습니까?

![image](https://user-images.githubusercontent.com/61821641/148649338-46ef18a3-db9a-46fb-b008-90f5c1900dbe.png)

답 : Windows user


3. Windows 문제해결 Command는 무엇입니까?

![image](https://user-images.githubusercontent.com/61821641/148649384-cbe2bbc1-ff60-44aa-b125-c5dc8b54a406.png)

답 : C:\Windows\System32\control.exe /name Microsoft.Troubleshooting

4. 제어판을 여는 명령어는 무엇입니까?

![image](https://user-images.githubusercontent.com/61821641/148649474-df22bc7a-2dd7-416a-a4bb-275d8d7e4b0a.png)

답 : control.exe

#
UAC 설정변경
바를 움직여 설정을 변경할 수 있다.

![image](https://user-images.githubusercontent.com/61821641/148650231-4d840a4d-15a9-4973-b963-6c7a2a88a070.png)

다음과 같은 경우 항상 알림:

- 앱이 소프트웨어를 설치하거나 내 컴퓨터를 변경하려고 시도.
- Windows 설정을 변경.

정기적으로 새 소프트웨어를 설치하고 익숙하지 않은 웹 사이트를 방문하는 경우 권장

![image](https://user-images.githubusercontent.com/61821641/148650420-485b162f-a7d4-4ecb-ba39-296d24a9ad94.png)

앱이 내 컴퓨터를 변경하려고 할 때 알림(기본값):
- Windows 설정을 변경할 때 알리지 않음

![image](https://user-images.githubusercontent.com/61821641/148650430-2557ea75-1757-4014-9031-803fafe9dc18.png)

앱이 컴퓨터를 변경하려고 할 때 알림(바탕 화면을 어둡게 하지 않음):

-  Windows 설정을 변경할 때 알리지 않음

![image](https://user-images.githubusercontent.com/61821641/148650487-95477fc6-b843-4762-927c-2a3b6252dc08.png)

다음 경우 알리지 않음:
- 앱이 소프트웨어를 설치하거나 내 컴퓨터를 변경하려고 시도.
- Windows 설정을 변경.
#
문제

1. UAC를 여는 명령어는?

![image](https://user-images.githubusercontent.com/61821641/148650185-28a3edba-91de-44a2-ae3a-902775adb172.png)

답 : UserAccountControlSettings.exe

#
Computer Management


컴퓨터관리 `compmgmt` 유틸리티에는 시스템 도구, 저장소, 서비스 및 응용프로그램 탭이 있다.

시스템 도구

- 작업스케줄러

![image](https://user-images.githubusercontent.com/61821641/148650711-9d09a590-678f-4429-9b63-febdb050ab7b.png)

작업스케줄러를 이용하여 컴퓨터가 지정한 시간에 자동으로 수행할 작업을 관리할 수 있다.

생성: `Create Basic Task`를 클릭해서 생성

- 이벤트 뷰어

![image](https://user-images.githubusercontent.com/61821641/148650861-c092d408-981b-457c-b561-9d5624aa2b3a.png)

컴퓨터에서 발생한 다양한 이벤트를 볼 수 있다. 이 정보로 문제를 진단하고 시스템의 작업을 추적할 수 있다.

기록할 수 있는 이벤트 유형(참고:[docs.microsoft.com](https://docs.microsoft.com/en-us/windows/win32/eventlog/event-types))
| **Event Type** | Description |
| --- | --- |
| **Error** | 데이터 손실 또는 기능 손실과 같은 심각한 문제를 나타내는 이벤트입니다. 예를 들어 시작하는 동안 서비스가 로드되지 않으면 오류 이벤트가 기록됩니다. |
| **Warning** | 반드시 중요하지는 않지만 잠재적인 미래 문제를 나타낼 수 있는 이벤트입니다. 예를 들어 디스크 공간이 부족하면 경고 이벤트가 기록됩니다. 애플리케이션이 기능이나 데이터의 손실 없이 이벤트에서 복구할 수 있는 경우 일반적으로 이벤트를 경고 이벤트로 분류할 수 있습니다. |
| **Information** | 애플리케이션, 드라이버 또는 서비스의 성공적인 작동을 설명하는 이벤트입니다. 예를 들어, 네트워크 드라이버가 성공적으로 로드되면 정보 이벤트를 기록하는 것이 적절할 수 있습니다. 데스크톱 응용 프로그램이 시작할 때마다 이벤트를 기록하는 것은 일반적으로 부적절합니다. |
| **Success Audit** | 감사된 보안 액세스 시도를 기록하는 이벤트입니다. 예를 들어, 사용자의 성공적인 시스템 로그온 시도는 성공 감사 이벤트로 기록됩니다. |
| **Failure Audit** | 실패한 감사된 보안 액세스 시도를 기록하는 이벤트입니다. 예를 들어, 사용자가 네트워크 드라이브에 액세스하려고 시도했지만 실패하면 실패 감사 이벤트로 기록됩니다. |

Windows 표준로그(참고: [docs.microsoft.com](https://docs.microsoft.com/en-us/windows/win32/eventlog/eventlog-key))

| **Log** | Description |
| --- | --- |
| **Application** | 애플리케이션에 의해 기록된 이벤트를 포함합니다. 예를 들어, 데이터베이스 응용 프로그램은 파일 오류를 기록할 수 있습니다. 애플리케이션 개발자는 기록할 이벤트를 결정합니다. |
| **Security** | 유효하거나 잘못된 로그온 시도와 같은 이벤트와 파일 또는 기타 개체 생성, 열기 또는 삭제와 같은 리소스 사용과 관련된 이벤트를 포함합니다. 관리자는 보안 로그에 이벤트를 기록하기 위해 감사를 시작할 수 있습니다. |
| **System** | 시작하는 동안 로드되지 않는 드라이버 또는 기타 시스템 구성 요소와 같은 시스템 구성 요소에 의해 기록된 이벤트를 포함합니다. |
| **CustomLog** | 사용자 지정 로그를 생성하는 애플리케이션에서 기록하는 이벤트를 포함합니다. 사용자 지정 로그를 사용하면 응용 프로그램이 다른 응용 프로그램에 영향을 주지 않고 보안 목적으로 로그 크기를 제어하거나 ACL을 연결할 수 있습니다. |

- 공유 폴더

![image](https://user-images.githubusercontent.com/61821641/148651196-bc98e7c0-9a05-4086-b155-b488910a4d6a.png)

공유폴더에는 다른사람이 연결할 수 있는 공유폴더 목록을 볼 수 있다.

![image](https://user-images.githubusercontent.com/61821641/148651239-623427c5-6754-4363-b3c3-64441dedd17c.png)

Windows의 기본 공유인 C$와 Windows에서 만든 기본 원격 관리 공유(예: ADMIN$)가 있다. 

![image](https://user-images.githubusercontent.com/61821641/148651300-76d13300-27ba-4fbf-9743-6eaeee9e7b3e.png)

마우스우클릭으로 properties에 들어가서 속성을 볼 수 있다.

sessions 하위에 현재 공유에 연결된 사용자 목록이 표시된다.
open files 하위에 연결된 사용자가 액세스하는 모든 폴더, 파일 목록이 표시된다.

- Local Users and Groups**

`lusrmgr.msc`

![image](https://user-images.githubusercontent.com/61821641/148651373-320bb69e-707f-4fb9-bf32-fe616c9dcd01.png)

`perfmon`

![image](https://user-images.githubusercontent.com/61821641/148651448-76c92f31-1fb0-4671-af2a-2e843ffe734a.png)

Performance Monitor는 실시간으로 또는 로그 파일에서 성능 데이터를 보는 데 사용된다. 이 유틸리티는 로컬이든 원격이든 컴퓨터 시스템의 성능 문제를 해결하는 데 유용하다. 

- 장치관리자

장치관리자를 사용하면 컴퓨터에 연결된 하드웨어를 비활성화하는 등 하드웨어를 보고 구성할 수 있다.

![image](https://user-images.githubusercontent.com/61821641/148651547-c62e587f-6951-4d5a-a5b5-16995d45b649.png)

- Storage

![image](https://user-images.githubusercontent.com/61821641/148651590-13e99e24-05c4-43fb-b8d7-ae87f7f34c27.png)

Storage 하위에는 Windows Server Backup, Disk Mangement가 있다.

Windows Server Backup

![image](https://user-images.githubusercontent.com/61821641/148651683-0e63d604-75c1-4391-880e-16a50e9c5ac1.png)

Disk Management

![image](https://user-images.githubusercontent.com/61821641/148651710-51d05410-99e1-417a-a7d3-7ce9398161ab.png)

디스크 관리는 고급 저장 작업을 수행할 수 있는 Windows의 시스템 유틸리티이다.  일부 작업은 다음과 같다.

> 새 드라이브 설정<br/>
파티션 확장<br/>
파티션 축소<br/>
드라이브 문자 할당 또는 변경(예: E:) <br/>

- Services and Application

![image](https://user-images.githubusercontent.com/61821641/148651817-1559f3b0-6336-45f0-ae6b-5efa4b089e3b.png)

서비스는 백그라운드에서 실행되는 응용 프로그램의 특별한 유형이다. 여기에서 서비스의 속성 보기와 같이 서비스를 활성화 및 비활성화하는 것 이상의 작업을 수행할 수 있다. 

![image](https://user-images.githubusercontent.com/61821641/148651880-02a51c8d-7e06-4c8e-914a-14356e3b2a10.png)

WMI 컨트롤은 WMI( Windows Management Instrumentation ) 서비스를 구성하고 제어한다 .

Wikipedia에 따르면 
> WMI는 VBScript 또는 Windows PowerShell과 같은 스크립팅 언어를 사용하여 로컬 및 원격에서 Microsoft Windows 개인용 컴퓨터 및 서버를 관리할 수 있습니다. Microsoft는 WMIC(Windows Management Instrumentation Command-line)라는 WMI에 대한 명령줄 인터페이스도 제공합니다.

참고 : WMIC 도구는 Windows 10 버전 21H1에서 더 이상 사용되지 않는다. Windows PowerShell은 WMI용으로 이 도구를 대체한다. 
#
문제

1. GoogleUpdateTaskMachineUA 작업이 매일 몇 시에 실행되도록 구성되어 있습니까?

![image](https://user-images.githubusercontent.com/61821641/148652110-de9c1330-4d52-4c9b-9bbb-75c06de93f95.png)

답 : 6:15 am

#
System Information

Microsoft에 따르면 
> Windows에는 Microsoft 시스템 정보(Msinfo32.exe)라는 도구가 포함되어 있습니다. 이 도구는 컴퓨터에 대한 정보를 수집하고 컴퓨터 문제를 진단하는 데 사용할 수 있는 하드웨어, 시스템 구성 요소 및 소프트웨어 환경에 대한 포괄적인 보기를 표시합니다. 

`msinfo32`

![image](https://user-images.githubusercontent.com/61821641/148652224-0468564a-b294-4ccc-86a0-990717c32afb.png)

시스템 요약에는 프로세서 브랜드 및 모델과 같은 컴퓨터의 일반적인 기술 사양이 표시된다.





#
reference

- [시스템 구성 유틸리티를 이용하여 구성오류를 해결하는 방법--ms docs](https://docs.microsoft.com/en-us/troubleshoot/windows-client/performance/system-configuration-utility-troubleshoot-configuration-errors)


