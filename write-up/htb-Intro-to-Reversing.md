Intro-to-Reversing
#
`Baby Re`
```
7z e 'Baby RE.zip' 
strings baby   
```
![image](https://user-images.githubusercontent.com/61821641/150638630-6b5e5e0b-d707-4a07-b3dd-383bfd5af163.png)

![image](https://user-images.githubusercontent.com/61821641/150638639-745a8300-0281-4ecb-a2a5-84b175cb1156.png)

#
`You Cant C Me`

```
ltrace ./auth  

```
![image](https://user-images.githubusercontent.com/61821641/150638673-1ded9480-9634-42f5-a83a-0500c7a8a12c.png)

![image](https://user-images.githubusercontent.com/61821641/150638688-e6a90457-65f2-47d5-8d9c-81b4116e062d.png)
#
