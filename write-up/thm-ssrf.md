SSRF(Server-Side Request Forgery) 
#
`SSRF란`

악의적인 사용자가 웹 서버가 공격자가 선택한 리소스에 대해 추가 또는 편집된 HTTP 요청을 하도록 하는 취약점입니다.

`SSRF 종류`

- 데이터가 공격자의 화면으로 반환되는 일반 SSRF
- SSRF가 발생했지만 공격자의 화면에 정보가 반환되지 않는 Blind SSRF

`SSRF 공격이 성공했을 때`

- 승인되지 않은 영역에 대한 액세스.
- 고객/조직 데이터에 대한 액세스.
- 내부 네트워크로 확장하는 기능.
- 인증 토큰/자격 증명을 공개.

아래에서 표시된 부분만 변경해도 user의 데이터를 얻을 수 있습니다.

![image](https://user-images.githubusercontent.com/61821641/150425784-c5131c88-740a-4b8e-be14-4c704fb4a177.png)

디렉토리 탐색을 사용하여 경로를 제어하는 것만으로 공격자가 /api/user 페이지에 도달할 수 있습니다. `../` 는 요청의 /stock 부분을 제거하고 최종 요청을 /api/user로 바꾸는 디렉토리를 위로 이동하라는 메시지입니다.

![image](https://user-images.githubusercontent.com/61821641/150426265-b03534b3-ea8e-4b5c-a02b-312c5f5f0a0f.png)

공격자는 요청이 생성되는 서버의 하위 도메인을 제어할 수 있습니다. &x=로 끝나는 페이로드는 공격자의 URL 끝에 나머지 경로가 추가되는 것을 중지하고 대신 쿼리 문자열에서 매개 변수(?x=)로 변환하는 데 사용됩니다.

![image](https://user-images.githubusercontent.com/61821641/150426525-52555295-4479-4e9d-82a3-4aa814c12f7d.png)

원래 요청으로 돌아가서 공격자는 대신 웹 서버가 공격자가 선택한 서버를 요청하도록 강제할 수 있습니다. 이렇게 하면 공격자의 지정된 도메인으로 전송되는 요청 헤더를 캡처할 수 있습니다. 이러한 헤더에는 인증 자격 증명 또는 website.thm(일반적으로 api.website.thm에 인증됨)에서 보낸 API 키가 포함될 수 있습니다.

![image](https://user-images.githubusercontent.com/61821641/150427301-f99c443e-7729-4c26-bb96-f02bcc8c4c0f.png)


```
https://website.thm/item/2?server=server.website.thm/flag?id=9&x=
```
위의 주소로 요청하면 아래와 같이 server에 요청하게 되는 사이트입니다. &x=뒤의 경로는 무시되기 때문에 `https://server.website.thm/flag?id=9` 사이트가 요청되었습니다.
```
Server Requesting: https://server.website.thm/flag?id=9&x=.website.thm/api/item?id=2
```

#
`SSRF 찾기`

1. 주소 표시줄의 매개변수에 전체 URL이 사용되는 경우:

![image](https://user-images.githubusercontent.com/61821641/150429396-756945f5-c328-43ca-8e2e-7ccf8a063ef8.png)

2. form의 숨겨진 필드:

![image](https://user-images.githubusercontent.com/61821641/150429471-2bb65368-9bb6-4477-b962-1b9d4303c753.png)

3. 호스트 이름과 같은 부분 URL:

![image](https://user-images.githubusercontent.com/61821641/150429553-6aa08452-adc1-496b-9a0a-a99c1d4058bf.png)

4. URL의 경로만:

![image](https://user-images.githubusercontent.com/61821641/150429591-81271b8a-c905-403e-abd4-3a1d8856e918.png)

출력이 다시 반영되지 않는 블라인드 SSRF로 작업하는 경우 [requestbin.com](https://requestbin.com/), 자체 HTTP 서버 또는 Burp Suite의 Collaborator 클라이언트와 같은 요청을 모니터링하기 위해 외부 HTTP 로깅 도구를 사용해야 합니다.

#
`일반적인 SSRF 방어 제거하기`

SSRF 취약점의 위험을 알고 있는 보안에 정통한 개발자는 요청된 리소스가 특정 규칙을 충족하는지 확인하기 위해 애플리케이션에서 검사를 구현할 수 있습니다. Deny List, Allow List의 두 가지 접근 방식이 있습니다.

`Deny List` -- 특정 입력만 거부

거부 목록은 목록에 지정되거나 특정 패턴과 일치하는 리소스를 제외한 모든 요청이 수락되는 곳입니다. 웹은 민감한 엔드포인트, IP 주소, 도메인이 다른 위치에 대한 액세스를 계속 허용하면서 대중이 액세스하지 못하도록 보호하기 위해 거부 목록을 사용할 수 있습니다. 액세스를 제한하는 특정 끝점은 `localhost`이며 여기에는 서버 성능 데이터 또는 추가 민감한 정보가 포함될 수 있으므로 localhost, 127.0.0.1과 같은 도메인 이름이 거부 목록에 나타납니다. 공격자는 `0, 0.0.0.0, 0000, 127.1, 127.*.*.*, 2130706433, 017700000001, IP 주소 0.17로 확인되는 DNS 레코드가 있는 하위 도메인`과 같은 대체 로컬 호스트 참조를 사용하여 거부 목록을 우회할 수 있습니다. 예: 127.0.0.1.nip.io.

또한 클라우드 환경에서는 민감한 정보를 포함하여 배포된 클라우드 서버에 대한 메타데이터가 포함된 IP 주소 `169.254.169.254`에 대한 액세스를 차단하는 것이 좋습니다. 공격자는 IP 주소 169.254.169.254를 가리키는 DNS 레코드를 사용하여 자신의 도메인에 하위 도메인을 등록하여 이를 우회할 수 있습니다.

`Allow List` -- 특정 입력만 허용

허용 목록은 목록에 나타나지 않거나 매개변수에 사용된 URL이 `https://website.thm` 으로 시작해야 한다는 규칙과 같이 특정 패턴과 일치하지 않는 한 모든 요청이 거부되는 곳입니다.  공격자는 `https://website.thm.attackers-domain.thm`과 같은 공격자의 도메인 이름에 하위 도메인을 만들어 이 규칙을 빠르게 우회할 수 있습니다. 애플리케이션 로직은 이제 이 입력을 허용하고 공격자가 내부 HTTP 요청을 제어할 수 있도록 합니다.

`Open Redirect`

위의 우회가 작동하지 않으면 공격자의 슬리브를 가로막는 또 하나의 트릭인 개방형 리디렉션이 있습니다. 개방형 리디렉션은 웹 사이트 방문자가 자동으로 다른 웹 사이트 주소로 리디렉션되는 서버의 끝점입니다. 예를 들어 링크 `https://website.thm/link?url=https://tryhackme.com`을 사용하십시오. 이 끝점은 방문자가 광고/마케팅 목적으로 이 링크를 클릭한 횟수를 기록하기 위해 만들어졌습니다. 그러나 `https://website.thm/`으로 시작하는 URL만 허용하는 엄격한 규칙과 함께 잠재적인 SSRF 취약점이 있다고 상상해 보십시오. 공격자는 위의 기능을 활용하여 내부 HTTP 요청을 공격자가 선택한 도메인으로 리디렉션할 수 있습니다.

#
`SSRF 실용`

```
nmap -sC -sV 10.10.57.146
```

/private에 접근하기

![image](https://user-images.githubusercontent.com/61821641/150433711-066bc57d-9a26-4e52-b813-a7df7a008da7.png)

![image](https://user-images.githubusercontent.com/61821641/150435639-70abc1ed-b980-433c-8b0a-c0df5dd90a83.png)

form submit하면 아래와 같은 결과를 얻을 수 있다.

![image](https://user-images.githubusercontent.com/61821641/150435953-67e5be54-0d3f-4cfb-bf4a-21a10cc4eb85.png)

![image](https://user-images.githubusercontent.com/61821641/150435907-2323e1f8-fc52-4869-93f7-702fe949fda6.png)

